//SPDX-License-Identifier: MIT
pragma solidity 0.8.6;

contract ArraysAssignment
{

    uint[] Arr = [23, 45, 67, 88, 98, 65];

    // append element to the array
    function updateArray(uint num) public
    {
        Arr.push(num);
    }
    
    // get the array
    function getArray() public view returns (uint[] memory)
    {
        return Arr;
    }

    // remove last element from the array
    function popArray() public
    {
        Arr.pop();
    }

    // get length of array
    function getLength() public view returns (uint) 
    {
        return Arr.length;
    }

    // remove element from specific index
    function remIndEle(uint ind) public
    {
        delete Arr[ind];
    }

    // after removing any element from middle of an array shift all the element to left
    function completeAssign() public returns (uint)
    {
        uint targetInd;
        for(uint i=0; i<Arr.length; i++)
        {
            uint myEle = Arr[i];
            if(myEle == 0)
            {
                targetInd = i;
                break;
            }
        }

        for(uint i=targetInd; i<Arr.length; i++)
        {
            if(i == (Arr.length-1))
            {
                Arr.pop();
            }
            else
            {
                Arr[i] = Arr[i+1];
            }
        }            
    }

}
